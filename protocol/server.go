package protocol

import (
	"fmt"
	"io"
	"github.com/fxamacker/cbor/v2"
	"github.com/containerssh/log"
)

type ConnectionHandler struct {
	SingleConnection      bool
	BindHost              string
	BindPort              string
	Protocol              string
	ToAgent               io.Writer
	FromAgent             io.Reader
	ConnectionCallback    func(channelId int, connectedAddress string, connectedPort uint32, origAddress string, origPort uint32) (io.Writer, io.Reader, func(), error)
	Logger                log.Logger

	connMap               map[int]connection
	encoder               *cbor.Encoder
	decoder               *cbor.Decoder
}

type connection struct {
	connectionId     int
	toClient         io.Writer
	fromClient       io.Reader
	toAgent          *cbor.Encoder
	closeCallback    func()
	logger           log.Logger
}

func checkSuccess(decoder *cbor.Decoder) error {
	packet := Packet{}
	err := decoder.Decode(&packet)
	if err != nil {
		return err
	}

	if packet.Type == PACKET_ERROR {
		return fmt.Errorf("Error received from agent after setup")
	} else if packet.Type != PACKET_SUCCESS {
		return fmt.Errorf("Invalid packet received, expecting error/success")
	}
	return nil
}

func (c *connection) forwardToAgent() {
	buf := make([]byte, 8192)
	for {
		nBytes, err := c.fromClient.Read(buf)
		if err != nil {
			return
		}
		c.logger.Debug(log.NewMessage("SND", "Sending PACKET_DATA"))
		packet := Packet{
			Type: PACKET_DATA,
			ConnectionId: c.connectionId,
			Payload: buf[0:nBytes],
		}
		err = c.toAgent.Encode(&packet)
		if err != nil {
			return
		}
	}
}

func (h *ConnectionHandler) handleNewConnection(packet Packet, connectionId int) error {
	payload := NewConnectionPayload{}
	err := cbor.Unmarshal(packet.Payload, &payload)
	if err != nil {
		return err
	}

	writer, reader, closeFunc, err := h.ConnectionCallback(
		connectionId,
		payload.ConnectedAddress,
		payload.ConnectedPort,
		payload.OriginatorAddress,
		payload.OriginatorPort,
	)
	if err != nil {
		packet := Packet{
			Type: PACKET_ERROR,
		}
		h.encoder.Encode(&packet)
		return err
	}
	success := Packet{
		Type: PACKET_SUCCESS,
		ConnectionId: connectionId,
	}
	h.encoder.Encode(&success)

	conn := connection{
		connectionId: connectionId,
		toClient: writer,
		fromClient: reader,
		toAgent: h.encoder,
		closeCallback: closeFunc,
		logger: h.Logger,
	}
	h.connMap[connectionId] = conn

	go conn.forwardToAgent()

	return nil
}

func (h *ConnectionHandler) handleData(packet Packet) error {
	if conn, ok := h.connMap[packet.ConnectionId]; ok {
		_, err := conn.toClient.Write(packet.Payload)
		if err != nil {
			return err
		}
		return nil
	}
	return fmt.Errorf("Invalid connection")
}

func (h *ConnectionHandler) handleCloseConnection(packet Packet) error {
	if conn, ok := h.connMap[packet.ConnectionId]; ok {
		delete(h.connMap, packet.ConnectionId)
		conn.closeCallback()
	}
	return fmt.Errorf("Invalid connection")
}

func (h *ConnectionHandler) Listen(setupPayload []byte) error {
	_, err := h.ToAgent.Write(setupPayload)
	if err != nil {
		return err
	}

	h.encoder = cbor.NewEncoder(h.ToAgent)
	h.decoder = cbor.NewDecoder(h.FromAgent)
	err = checkSuccess(h.decoder)
	if err != nil {
		return err
	}

	h.connMap = make(map[int]connection)

	nextConnectionId := 1
	for {
		packet := Packet{}
		err := h.decoder.Decode(&packet)
		if err != nil {
			return err
		}
		switch packet.Type {
		case PACKET_NEW_CONNECTION:
			h.Logger.Debug(log.NewMessage("RECV", "Received PACKET_NEW_CONNECTION"))
			err := h.handleNewConnection(packet, nextConnectionId)
			if err != nil {
				return err
			}
			nextConnectionId++
		case PACKET_DATA:
			h.Logger.Debug(log.NewMessage("RECV", "Received PACKET_DATA"))
			err := h.handleData(packet)
			if err != nil {
				return err
			}
		case PACKET_CLOSE_CONNECTION:
			h.Logger.Debug(log.NewMessage("RECV", "Received PACKET_CLOSE_CONNECTION"))
			err := h.handleCloseConnection(packet)
			if err != nil {
				return err
			}
		default:
			h.Logger.Debug(log.NewMessage("INVALID_PACKET", "Invalid packet received"))
		}
	}

	return nil
}
