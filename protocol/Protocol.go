package protocol

const (
	xauth_path = "/usr/bin/xauth"
)

const (
	CONNECTION_TYPE_X11 = iota
)

const (
	PACKET_SETUP = iota
	PACKET_SUCCESS
	PACKET_ERROR
	PACKET_DATA
	PACKET_NEW_CONNECTION
	PACKET_CLOSE_CONNECTION
	PACKET_CLOSE_SESSION
)

type SetupPacket struct {
	Display          string
	SingleConnection bool
	AuthProtocol     string
	AuthCookie       string
}

type NewConnectionPayload struct {
	ConnectedAddress  string
	ConnectedPort     uint32
	OriginatorAddress string
	OriginatorPort    uint32
}

type Packet struct {
	Type int
	ConnectionId int
	Payload []byte
}
