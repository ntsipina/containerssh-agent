module github.com/containerssh/agent

go 1.16

require (
	github.com/containerssh/log v1.1.6 // indirect
	github.com/fxamacker/cbor/v2 v2.3.0
)
