package main

import (
	"os"
	"io"
	"net"
	"fmt"
	oexec "os/exec"
	"github.com/fxamacker/cbor/v2"
	proto "github.com/containerssh/agent/protocol"
)

const (
	xauth_path = "/usr/bin/xauth"
)

func checkCreateXAuthority() error {
	file, err := os.OpenFile(".Xauthority", os.O_RDONLY|os.O_CREATE, 0644)
	if err != nil {
		return err
	}
	return file.Close()
}

func handleError(err error, i string) {
	file, _ := os.OpenFile(".err" + i, os.O_APPEND|os.O_RDWR|os.O_CREATE, 0644)
	_, _ = file.Write([]byte(err.Error()))
	file.Close()
}

func handleString(err string, i string) {
	file, _ := os.OpenFile(".msg" + i, os.O_APPEND|os.O_RDWR|os.O_CREATE, 0644)
	_, _ = file.Write([]byte(err))
	file.Close()
}

type x11Connection struct {
	conn net.Conn
	encoder *cbor.Encoder
	decoder *cbor.Decoder
}

func (c *x11Connection) handleLocalToRemote(connectionId int) {
	buf := make([]byte, 8192)

	for {
		nBytes, err := c.conn.Read(buf)
		if err != nil {
			handleError(err, "lr1")
			return
		}
		handleString("Wrote packet", "lr3")
		packet := proto.Packet{
			Type: proto.PACKET_DATA,
			ConnectionId: connectionId,
			Payload: buf[0:nBytes],
		}
		err = c.encoder.Encode(packet)
		if err != nil {
			handleError(err, "lr2")
			return
		}
		//packet := proto.DataPacket{
		//	Data: buf[0:nBytes],
		//}
		//mar, err := cbor.Marshal(packet)
		//if err != nil {
		//	return
		//}
		//wrapper, err := proto.NewPacket(proto.PACKET_DATA, mar)
		//if err != nil {
		//	return
		//}
		//c.encoder.Encode(wrapper)
	}
}

func (c *x11Connection) handleRemoteToLocal() {
	for {
		packet := proto.Packet{}
		err := c.decoder.Decode(&packet)
		if err != nil {
			handleError(err, "rl1")
			return
		}
		handleString("Read Packet", "rl3")
		if packet.Type != proto.PACKET_DATA {
			handleString("Incorrect packet type", "rl2")
			return
		}
		_, err = c.conn.Write(packet.Payload)
		if err != nil {
			handleError(err, "rl2")
			return
		}
	}
}

func (c *x11Connection) handleConnection(connectionId int) {
	go c.handleRemoteToLocal()
	c.handleLocalToRemote(connectionId)
	packet := proto.Packet{
		Type: proto.PACKET_CLOSE_CONNECTION,
		ConnectionId: connectionId,
	}
	c.encoder.Encode(packet)
}

func x11Forward(stdin io.Reader, stdout io.Writer, stderr io.Writer, exit exitFunc) {
	decoder := cbor.NewDecoder(stdin)
	encoder := cbor.NewEncoder(stdout)

	setup := proto.SetupPacket{}
	err := decoder.Decode(&setup)
	if err != nil {
		handleError(err, "1")
		exit(1)
	}

	checkCreateXAuthority()
	// TODO: Validate contents
	//exec.Command(xauth_path, "remove", setup.Display)
	cmd := oexec.Command(xauth_path, "add", setup.Display, setup.AuthProtocol, setup.AuthCookie)
	err = cmd.Run()
	if err != nil {
		handleError(err, "2")
		panic(err)
	}

	success := proto.Packet{
		Type: proto.PACKET_SUCCESS,
	}
	err = encoder.Encode(success)
	if err != nil {
		handleError(err, "2.5")
		panic(err)
	}

	sock, err := net.Listen("tcp", "127.0.0.1:6010")
	if err != nil {
		handleError(err, "3")
		_, _ = stderr.Write([]byte(err.Error()))
		exit(1)
	}

	for {
		conn, err := sock.Accept()
		if err != nil {
			handleError(err, "4")
			_, _ = stderr.Write([]byte(err.Error()))
			continue
		}
		x11conn := x11Connection{
			conn: conn,
			encoder: encoder,
			decoder: decoder,
		}
		addr := conn.RemoteAddr().(*net.TCPAddr).IP.String()
		port := conn.RemoteAddr().(*net.TCPAddr).Port
		connInfo := proto.NewConnectionPayload{
			OriginatorAddress: addr,
			OriginatorPort: uint32(port),
		}
		marInfo, err := cbor.Marshal(&connInfo)
		if err != nil {
			handleError(err, "5")
			panic(err)
		}
		packet := proto.Packet{
			Type: proto.PACKET_NEW_CONNECTION,
			Payload: marInfo,
		}
		err = encoder.Encode(&packet)
		if err != nil {
			handleError(err, "6")
			panic(err)
		}

		packet = proto.Packet{}
		err = decoder.Decode(&packet)
		if err != nil {
			handleError(err, "7")
		}
		if packet.Type == proto.PACKET_ERROR {
			handleString("received " + fmt.Sprintf("%d", packet.Type), "8")
			return
		} else if packet.Type != proto.PACKET_SUCCESS {
			handleString("received " + fmt.Sprintf("%d", packet.Type), "9")
			return
		}

		handleString("Accepted conn", "main1")
		x11conn.handleConnection(packet.ConnectionId)
		return
	}
}
